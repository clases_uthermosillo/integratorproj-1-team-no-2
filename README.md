# INTEGRATOR PROJECT I

## 3er 4 Month Term - Afternoon - UTHermosillo

---

# TEAM #2

#### Project: NotiMundo

---

### Welcome to your REPO in GitLab, so your work will be allocated, stored, and monitored here in on place.

#### You have to include the same data that is prepared for the base documentation:

- Description of the Problem to be Solved
- History of the problem
- Target market served by the solution
- Identification of the problem (wording in question form)
- Solution Statement
- General objective
- Project justification

## TEAM MEMBERS

### You list the team members here:
                                                gitlabUser  UTHermosilloMail for each please

- Team member #1 David Edgardo Garcia Salazar   @a20311017  a20311017@uthermosillo.edu.mx 
- Team member #2 Andres Raso Ramirez            @Student-Raso   a20311079@uthermosillo.edu.mx

* Tema member #3
